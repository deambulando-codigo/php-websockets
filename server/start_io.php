<?php
use Workerman\Worker;
use Workerman\WebServer;
use Workerman\Autoloader;
use PHPSocketIO\SocketIO;

// composer autoload
require_once join(DIRECTORY_SEPARATOR, array(__DIR__, "vendor", "autoload.php"));

// SSL Required - Production
$context_dev = array();
$context_dist = array(
  'ssl' => array(
    // 'local_cert'  => '/home/humos/ssl/public.pem',
    'local_cert'  => '/home/humos/ssl/certs/midiamagico_humo_sc_c0ff5_2071d_1642204799_6bc0f7bb88f50dcf004d5f87757e609b.crt',
    'local_pk'    => '/home/humos/ssl/keys/c0ff5_2071d_8cbb4f6ac2ccb2c1f39f965172c8c531.key',
    'verify_peer' => false
  )
);
$io = new SocketIO(2021, $context_dev); // $port, $context (Production)
$io->on('connection', function($socket){
    $socket->addedUser = false;
    // when the client emits 'new message', this listens and executes
    $socket->on('new message', function ($message)use($socket){
        // we tell the client to execute 'new message'
        $group_user = $socket->group_user;
        $id_internal = $socket->id;
        if(!empty($group_user)) {
            $socket->to($group_user)->emit('new message', array(
                'username'=> $socket->username,
                'message'=> $message,
            ));
        } else {
            $socket->broadcast->emit('new message', array(
                'username'=> $socket->username,
                'message'=> $message,
                'id' => $id_internal,
            ));
        }
    });

    // when the client emits 'add user', this listens and executes
    $socket->on('add user', function ($username, $reconnect=null) use($socket){
        if ($socket->addedUser)
            return;

        global $usernames, $numUsers;

        if(!empty($reconnect)) {
            $id_older = $reconnect;
            $socket->broadcast->emit('user left', array(
                'id' => $id_older,
                'username' => $socket->username,
                'numUsers' => $numUsers
             ));
        }
        // we store the username in the socket session for this client
        $socket->username = $username;
        $socket->id_user = $socket->id;
        $usernames[$socket->id] = [
            'id' => $socket->id,
            'username' => $socket->username
        ];
        ++$numUsers;
        $socket->addedUser = true;
        $socket->emit('login', array(
            'id' => $socket->id,
            'usernames' => $usernames,
            'numUsers' => $numUsers
        ));
        // echo globally (all clients) that a person has connected
        $socket->broadcast->emit('user joined', array(
            'id' => $socket->id,
            'username' => $socket->username,
            'numUsers' => $numUsers
        ));
    });

    $socket->on('left group', function ($group) use($socket) {
        if(is_array($group)) {
            sort($group);
            $group = join('_', $group);
        }
        $socket->group_user = null;
        $socket->leave($group);
    });

    $socket->on('join group', function ($group) use($socket) {
        if (is_array($group)) {
            sort($group);
            $group = join('_', $group);
        }
        $socket->group_user = $group;
        $socket->join($group);
    });

    // when the client emits 'typing', we broadcast it to others
    $socket->on('typing', function ($isgroup) use($socket) {
        $id_internal = $socket->id;
        if(!empty($isgroup) && $isgroup === 'si') {
            $socket->to($isgroup)->emit('prueba typing', array(
                'username' => $socket->username,
                'isgroup' => $isgroup,
            ));
            $socket->to($isgroup)->emit('typing', array(
                'username' => $socket->username,
                'isgroup' => $isgroup,
            ));
        } else {
            $socket->broadcast->emit('typing', array(
                'username' => $socket->username,
                'id' => $id_internal,
            ));
        }
    });

    // when the client emits 'stop typing', we broadcast it to others
    $socket->on('stop typing', function () use($socket) {
        $socket->broadcast->emit('stop typing', array(
            'username' => $socket->username
        ));
    });

    // when the user disconnects.. perform this
    $socket->on('disconnect', function () use($socket) {
        global $usernames, $numUsers;
        if($socket->addedUser) {
            --$numUsers;
            unset($usernames[$socket->id]);

           // echo globally that this client has left
           $socket->broadcast->emit('user left', array(
               'id' => $socket->id,
               'username' => $socket->username,
               'numUsers' => $numUsers
            ));
        }
   });
   
});

if (!defined('GLOBAL_START')) {
    Worker::runAll();
}
