<?php

use Workerman\Worker;

require_once __DIR__ . '/vendor/autoload.php';

$context_dev = array();
// SSL context.
$context_dist = array(
    'ssl' => array(
			'local_cert'  => 'C:\laragon\etc\ssl\laragon.crt',
        'local_pk'    => 'C:\laragon\etc\ssl\laragon.key',
        'verify_peer' => false,
    )
);

// Create a Websocket server with ssl context.
$ws_worker = new Worker('websocket://0.0.0.0:2021', $context_dist);
$ws_worker->name = 'Barabasto'; // enable in distribution mode

// Enable SSL. WebSocket+SSL means that Secure WebSocket (wss://). 
// The similar approaches for Https etc.
$ws_worker->transport = 'ssl'; // enable in distribution mode

$tcp_worker->onConnect = function ($connection) {
	echo "New Connection\n";
};

$ws_worker->onMessage = function ($connection, $data) {
    // Send hello $data
    $connection->send('Hello ' . $data);
};

$tcp_worker->onClose = function ($connection) {
	echo "Connection closed\n";
};

Worker::runAll();