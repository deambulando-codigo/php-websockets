$(function () {
	var FADE_TIME = 150; // ms
	var TYPING_TIMER_LENGTH = 400; // ms
	var COLORS = [
		"#e21400",
		"#91580f",
		"#f8a700",
		"#f78b00",
		"#58dc00",
		"#287b00",
		"#a8f07a",
		"#4ae8c4",
		"#3b88eb",
		"#3824aa",
		"#a700ff",
		"#d300e7",
	];

	// Initialize variables
	var $window = $(window);
	var $usernameInput = $(".usernameInput"); // Input for username
	var $messages = $(".messages"); // Messages area
	var $inputMessage = $(".inputMessage"); // Input message input box
	var $isgroup = $("#isgroup"); // Input message input box

	var $loginPage = $(".login.page"); // The login page
	var $chatPage = $(".chat.page"); // The chatroom page

	// Prompt for setting a username
	var username, id_user;
	var connected = false;
	var typing = false;
	var lastTypingTime;
	var $currentInput = $usernameInput.focus();

	var protocol = location.protocol || "http:";
	var protocol_ws = (location.protocol).indexOf("https") !== -1 ? 'wss:' : "ws:";
	var socket = io(protocol_ws + "//" + document.domain + ":2021", {
		id: "c3d3dee52d74d841017227d9",
		path: '/barabasto'
	});
	console.log(socket);

	// const websocket = new WebSocket(
	// 	"wss://demo.piesocket.com/v3/channel_1?api_key=oCdCMcMPQpbvNjUIzqtvF1d2X2okWpDQj4AwARJuAgtjhzKxVEjQU6IdCjwm&notify_self"
	// 	// "ws:" + "//192.168.0.122:2021" + "/socket.io"
	// );
	// console.log(websocket);

	const addParticipantsMessage = (data) => {
		var message = "";
		if (data.numUsers === 1) {
			message += "Hay 1 participante";
		} else {
			message += "Hay " + data.numUsers + " participantes";
		}
		log(message);
	};

	const addPrivateMessage = ({id, username} = {}) => {
		const _html = `<div class="btn btn-outline-primary cntUser col mx-2 my-2" data-id="${id}">
    <span class="_username">${username}</span>
    </div>`;
		$(".cntInputMessage .cntUsers").append(_html);
	};

	const removePrivateMessage = ({id} = {}) => {
		$(".cntInputMessage .cntUsers")
			.find(".cntUser")
			.filter(`[data-id="${id}"]`)
			.fadeOut(400, () => $(this).remove());
	};

	// Sets the client's username
	const setUsername = () => {
		username = cleanInput($usernameInput.val().trim());

		// If the username is valid
		if (username) {
			$loginPage.fadeOut();
			$chatPage.show(200, function () {
				$(this).css("display", "flex");
			});
			$loginPage.off("click");
			$currentInput = $inputMessage.focus();

			// Tell the server your username
			socket.emit("add user", username);
		}
	};

	// Sends a chat message
	const sendMessage = () => {
		var message = $inputMessage.val();
		// Prevent markup from being injected into the message
		message = cleanInput(message);
		// if there is a non-empty message and a socket connection
		if (message && connected) {
			const is_group = $("#isgroup").is(":checked") ? "si" : "no";
			$inputMessage.val("");
			addChatMessage({
				_me: true,
				username: username,
				message: message,
			});
			// tell server to execute 'new message' and send along one parameter
			socket.emit("new message", message, is_group);
		}
	};

	// Log a message
	const log = (message, options) => {
		var $el = $("<p>").addClass("log").text(message);
		addMessageElement($el, options);
	};

	// Adds the visual chat message to the message list
	const addChatMessage = (data, options) => {
		// Don't fade the message in if there is an 'X was typing'
		var $typingMessages = getTypingMessages(data);
		options = options || {};
		if ($typingMessages.length !== 0) {
			options.fade = false;
			$typingMessages.remove();
		}

		var $usernameDiv = $('<span class="username"/>')
			.text(data.username)
			.css("color", getUsernameColor(data.username));
		var $messageBodyDiv = $('<span class="messageBody">').text(data.message);

		var typingClass = data.typing ? "typing" : "";
		var myMessage = data?._me === true ? "_me" : "";
		var $messageDiv = $(`<div class="message ${myMessage}"/>`)
			.data("username", data.username)
			.addClass(typingClass)
			.append($usernameDiv, $messageBodyDiv);

		addMessageElement($messageDiv, options);
	};

	// Adds the visual chat typing message
	const addChatTyping = (data) => {
		console.log({type: "addChatTyping", data});
		data.typing = true;
		data.message = "está escribiendo...";
		addChatMessage(data);
	};

	// Removes the visual chat typing message
	const removeChatTyping = (data) => {
		getTypingMessages(data).fadeOut(function () {
			$(this).remove();
		});
	};

	// Adds a message element to the messages and scrolls to the bottom
	// el - The element to add as a message
	// options.fade - If the element should fade-in (default = true)
	// options.prepend - If the element should prepend
	//   all other messages (default = false)
	const addMessageElement = (el, options) => {
		var $el = $(el);

		// Setup default options
		if (!options) {
			options = {};
		}
		if (typeof options.fade === "undefined") {
			options.fade = true;
		}
		if (typeof options.prepend === "undefined") {
			options.prepend = false;
		}

		// Apply options
		if (options.fade) {
			$el.css("display", "inline-flex").hide().fadeIn(FADE_TIME);
		}
		if (options.prepend) {
			$messages.prepend($el);
		} else {
			$messages.append($el);
		}
		$messages[0].scrollTop = $messages[0].scrollHeight;
	};

	// Prevents input from having injected markup
	const cleanInput = (input) => {
		return $("<div/>").text(input).html();
	};

	// Updates the typing event
	const updateTyping = () => {
		if (connected) {
			if (!typing) {
				const is_group = $("#isgroup").is(":checked") ? "si" : "no";
				typing = true;
				socket.emit("typing", is_group);
			}
			lastTypingTime = new Date().getTime();

			setTimeout(() => {
				var typingTimer = new Date().getTime();
				var timeDiff = typingTimer - lastTypingTime;
				if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
					socket.emit("stop typing");
					typing = false;
				}
			}, TYPING_TIMER_LENGTH);
		}
	};

	// Gets the 'X is typing' messages of a user
	const getTypingMessages = (data) => {
		return $(".typing.message").filter(function (i) {
			return $(this).data("username") === data.username;
		});
	};

	// Gets the color of a username through our hash function
	const getUsernameColor = (username) => {
		// Compute hash code
		var hash = 7;
		for (var i = 0; i < username.length; i++) {
			hash = username.charCodeAt(i) + (hash << 5) - hash;
		}
		// Calculate color
		var index = Math.abs(hash % COLORS.length);
		return COLORS[index];
	};

	// Keyboard events

	$window.keydown((event) => {
		// Auto-focus the current input when a key is typed
		if (!(event.ctrlKey || event.metaKey || event.altKey)) {
			$currentInput.focus();
		}
		// When the client hits ENTER on their keyboard
		if (event.which === 13) {
			if (username) {
				sendMessage();
				socket.emit("stop typing");
				typing = false;
			} else {
				setUsername();
			}
		}
	});

	// $isgroup.on("change", () => {
	// 	const is_group = $isgroup.is(":checked") ? "si" : "no";
	// 	socket.emit("join_left group", is_group);
	// });

	$(".cntUsers").on("click", ".cntUser", function () {
		const old_current = $(".cntUser._active");
		const id_old_current =
			old_current.length > 0 ? old_current.data("id") : null;
		const new_current = $(this);
		const is_active = $(this).hasClass("_active") ? true : false;
		const id_current = is_active === false ? new_current.data("id") : null;
		if (is_active === true) {
			$(this)
				.removeClass("btn-primary _active")
				.addClass("btn-outline-primary");
		} else {
			$(".cntUser")
				.removeClass("btn-primary _active")
				.addClass("btn-outline-primary");
			$(this)
				.addClass("btn-primary _active")
				.removeClass("btn-outline-primary");
		}
		// console.log({is_active, id_user});

		if (id_old_current != null) {
			const _group = new Array(id_user, id_old_current);
			socket.emit("left group", _group);
		}

		if (id_current !== null) {
			const _group = new Array(id_user, id_current);
			socket.emit("join group", _group);
		}

		// socket.emit("join_left group", is_group);
	});

	$inputMessage.on("input", () => {
		updateTyping();
	});

	// Click events

	// Focus input when clicking anywhere on login page
	$loginPage.click(() => {
		$currentInput.focus();
	});

	// Focus input when clicking on the message input's border
	$inputMessage.click(() => {
		$inputMessage.focus();
	});

	// Socket events

	// Whenever the server emits 'login', log the login message
	socket.on("login", (data) => {
		connected = true;
		// Display the welcome message
		var message = "Bienvenido a Narnia";
		log(message, {
			prepend: true,
		});
		addParticipantsMessage(data);

		const usernames =
			typeof data?.usernames === "object"
				? Object.values(data?.usernames)
				: new Array();
		if (usernames?.length > 0) {
			usernames.forEach((elem) => {
				elem.username = elem.id == data.id ? "Yo" : elem.username;
				addPrivateMessage(elem);
			});
		}
		id_user = data.id;
	});

	// Whenever the server emits 'new message', update the chat body
	socket.on("new message", (data) => {
		addChatMessage(data);
	});

	// Whenever the server emits 'user joined', log it in the chat body
	socket.on("user joined", (data) => {
		log(data.username + " se ha unido");
		addParticipantsMessage(data);
		addPrivateMessage(data);
	});

	// Whenever the server emits 'user left', log it in the chat body
	socket.on("user left", (data) => {
		log(data.username + " se nos fue");
		addParticipantsMessage(data);
		removeChatTyping(data);
		removePrivateMessage(data);
	});

	// Whenever the server emits 'typing', show the typing message
	socket.on("prueba typing", (data) => {
		console.log({type: "prueba typing", data});
	});

	// Whenever the server emits 'typing', show the typing message
	socket.on("typing", (data) => {
		addChatTyping(data);
	});

	// Whenever the server emits 'stop typing', kill the typing message
	socket.on("stop typing", (data) => {
		removeChatTyping(data);
	});

	socket.on("disconnect", (data) => {
		log("Has sido desconectado");
	});

	socket.on("reconnect", () => {
		log("Reconectado");
		if (username) {
			socket.emit("add user", username, id_user);
		}
	});

	socket.on("reconnect_error", () => {
		log("Intento de reconexión fallido");
	});
});
