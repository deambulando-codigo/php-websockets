<!doctype html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Chat de Angel Ramirez</title>
  <link rel="stylesheet" href="css/style.css?v=1.0.1">
</head>
<body>
  <ul class="pages">
    <li class="chat page">
      <div class="chatArea">
        <ul class="messages"></ul>
      </div>
      <input class="inputMessage" placeholder="¿Qué estás pensando?"/>
    </li>
    <li class="login page">
      <div class="form">
        <h3 class="title">Explica cómo te llamas, sin decir tu nombre</h3>
        <input class="usernameInput" type="text" maxlength="14" />
      </div>
    </li>
  </ul>

  <script src="js/jquery.min.js"></script>
  <script src="js/socket.io-client/socket.io.js"></script>
  <script src="js/main.js?v=1.0.0"></script>
</body>
</html>
