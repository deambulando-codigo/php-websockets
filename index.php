<!doctype html>
<html lang="es">

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
		<title>Chat de Angel Ramirez</title>
		<link rel="shortcut icon" href="./favicon.ico" type="image/x-icon">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<!-- <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300;400;700&display=swap" rel="stylesheet"> -->
		<link rel="stylesheet" href="css/bootstrap.min.css?v=5.0.2">
		<link rel="stylesheet" href="css/style.css?v=1.0.3">
	</head>

	<body>
		<section class="pages">
			<div class="chat page">
				<div class="chatArea">
					<div class="messages"></div>
				</div>
				<div class="cntInputMessage">
					<!-- <label style="padding: 16px">
						<input type="checkbox" id="isgroup">
						<span>Privado</span>
					</label> -->
					<div class="cntUsers"></div>
					<input class="inputMessage" placeholder="¿Qué estás pensando?" />
				</div>
			</div>
			<div class="login page">
				<div class="form">
					<h3 class="title">Explica cómo te llamas, sin decir tu nombre</h3>
					<input class="usernameInput" type="text" maxlength="14" />
				</div>
			</div>
		</section>

		<script src="js/jquery.min.js"></script>
		<script src="js/socket.io-client/socket.io.js"></script>
		<script src="js/bootstrap.min.js?v=5.0.2"></script>
		<script src="js/main.js?v=1.0.3"></script>
	</body>

</html>