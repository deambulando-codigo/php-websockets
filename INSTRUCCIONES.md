# Generar certificado PEM a partir de un CRT
``` openssl x509 -inform PEM -in server.crt > public.pem ```

## Example Path's
``` /ssl/certs/midiamagico_humo_sc_be793_3514f_1629071999_91f48fd12d6e36bbf6f99c9419bacde4.crt ```

# Ejecutar el servidor solamente (sin pagina web)
```
php /home/humos/public_html/test/mi-dia-magico-web/phpsocket/server/start_io.php start -d
```